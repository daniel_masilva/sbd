package model;

import java.sql.Connection;
import java.sql.DriverManager;

public class Persistencia{
    private final String url ="jdbc:postgresql://localhost:5432/base_ingressaria";
    private final String usuario="postgres";
    private final String senha = "postgres";
    Connection con=null;
    
    public Connection getConexao(){
         try{
            Class.forName("org.postgresql.Driver");
            con=DriverManager.getConnection(url,usuario,senha);
            System.out.println("Conexão realizada com sucesso.");
        }catch(Exception e){
            System.err.println(e);
        }
        return con;
    }
        
    
    
    
}