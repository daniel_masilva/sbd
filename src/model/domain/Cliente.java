/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.domain;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author daniel
 */
public class Cliente implements Serializable{
    private int idUser;
    private String cpf;
    private char sexo;
    private String nome;
    private String dtNascimento;

    public Cliente() {
    }

    public Cliente(int idUser, String cpf, char sexo, String nome, String dtNascimento) {
        this.idUser = idUser;
        this.cpf = cpf;
        this.sexo = sexo;
        this.nome = nome;
        this.dtNascimento = dtNascimento;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDtNascimento() {
        return dtNascimento;
    }

    public void setDtNascimento(String dtNascimento) {
        this.dtNascimento = dtNascimento;
    }
    
    
   
}
