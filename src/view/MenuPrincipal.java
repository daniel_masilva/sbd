/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import control.Tela;
import java.awt.Color;
import javax.swing.BorderFactory;

/**
 *
 * @author Daniel
 */
public class MenuPrincipal extends javax.swing.JPanel {

    /**
     * Creates new form MenuPrincipal
     */
    public MenuPrincipal() {
        initComponents();

        this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        moradoresMenuButton = new javax.swing.JButton();
        funcionarioMenuButton = new javax.swing.JButton();
        financeiroMenuButton = new javax.swing.JButton();
        entradaSaidaMenuButton = new javax.swing.JButton();
        cadastroPontoLocalMenuButton = new javax.swing.JButton();
        infoPessoaisMenuButton = new javax.swing.JButton();
        logoutMenuButton = new javax.swing.JButton();
        TodasQueriesMenuButton = new javax.swing.JButton();

        moradoresMenuButton.setText("Cadastrar Cliente");
        moradoresMenuButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                moradoresMenuButtonActionPerformed(evt);
            }
        });

        funcionarioMenuButton.setText("Funcionários");
        funcionarioMenuButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                funcionarioMenuButtonActionPerformed(evt);
            }
        });

        financeiroMenuButton.setText("Financeiro");
        financeiroMenuButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                financeiroMenuButtonActionPerformed(evt);
            }
        });

        entradaSaidaMenuButton.setText("Entrada/Saída");
        entradaSaidaMenuButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                entradaSaidaMenuButtonActionPerformed(evt);
            }
        });

        cadastroPontoLocalMenuButton.setText("Cadastrar Ponto Local");
        cadastroPontoLocalMenuButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cadastroPontoLocalMenuButtonActionPerformed(evt);
            }
        });

        infoPessoaisMenuButton.setText("Inf. Pessoais");
        infoPessoaisMenuButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                infoPessoaisMenuButtonActionPerformed(evt);
            }
        });

        logoutMenuButton.setText("Logout");
        logoutMenuButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                logoutMenuButtonActionPerformed(evt);
            }
        });

        TodasQueriesMenuButton.setText("Todas as Queries");
        TodasQueriesMenuButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TodasQueriesMenuButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(TodasQueriesMenuButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(moradoresMenuButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(funcionarioMenuButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(financeiroMenuButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(entradaSaidaMenuButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cadastroPontoLocalMenuButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(infoPessoaisMenuButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(logoutMenuButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(moradoresMenuButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(funcionarioMenuButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(financeiroMenuButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(entradaSaidaMenuButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cadastroPontoLocalMenuButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(infoPessoaisMenuButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(TodasQueriesMenuButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(logoutMenuButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void moradoresMenuButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_moradoresMenuButtonActionPerformed
        Tela.atualizaTela("CadastroCliente");
    }//GEN-LAST:event_moradoresMenuButtonActionPerformed

    private void funcionarioMenuButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_funcionarioMenuButtonActionPerformed
       Tela.atualizaTela("Funcionario");
    }//GEN-LAST:event_funcionarioMenuButtonActionPerformed

    private void financeiroMenuButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_financeiroMenuButtonActionPerformed
       Tela.atualizaTela("Financeiro");
    }//GEN-LAST:event_financeiroMenuButtonActionPerformed

    private void entradaSaidaMenuButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_entradaSaidaMenuButtonActionPerformed
       Tela.atualizaTela("Financeiro");
    }//GEN-LAST:event_entradaSaidaMenuButtonActionPerformed

    private void cadastroPontoLocalMenuButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cadastroPontoLocalMenuButtonActionPerformed
       Tela.atualizaTela("CadastroPonto");
    }//GEN-LAST:event_cadastroPontoLocalMenuButtonActionPerformed

    private void infoPessoaisMenuButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_infoPessoaisMenuButtonActionPerformed
       Tela.atualizaTela("InformacaoPessoal");
    }//GEN-LAST:event_infoPessoaisMenuButtonActionPerformed

    private void logoutMenuButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_logoutMenuButtonActionPerformed

    }//GEN-LAST:event_logoutMenuButtonActionPerformed

    private void TodasQueriesMenuButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TodasQueriesMenuButtonActionPerformed
        Tela.atualizaTela("TodasQueries");
    }//GEN-LAST:event_TodasQueriesMenuButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton TodasQueriesMenuButton;
    private javax.swing.JButton cadastroPontoLocalMenuButton;
    private javax.swing.JButton entradaSaidaMenuButton;
    private javax.swing.JButton financeiroMenuButton;
    private javax.swing.JButton funcionarioMenuButton;
    private javax.swing.JButton infoPessoaisMenuButton;
    private javax.swing.JButton logoutMenuButton;
    private javax.swing.JButton moradoresMenuButton;
    // End of variables declaration//GEN-END:variables
}
