/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this temclate file, choose Tools | Temclates
 * and open the temclate in the editor.
 */
package control.classControl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Persistencia;
import model.domain.Cliente;

/**
 *
 * @author daniel
 */
public class ClienteControl {
    public static int insert(Cliente cl) throws SQLException{
        Persistencia per = new Persistencia();
        int qt;
        try (Connection conexao = per.getConexao()) {
            String sql = "insert into cliente (cpf, sexo, nome, dtNascimento) values (?,?,?,?)";
            try (PreparedStatement st = conexao.prepareStatement(sql)) {
                st.setString(1,cl.getCpf());
                st.setString(2, String.valueOf(cl.getSexo()));
                st.setString(3, cl.getNome());
                st.setDate(4,java.sql.Date.valueOf(cl.getDtNascimento()));
                qt = st.executeUpdate();
            }
        }
        return qt;
    }
    
    public static void remove(Cliente cl) throws SQLException{
        Persistencia per = new Persistencia();
        try (Connection conexao = per.getConexao()) {
            String sql = "delete from cliente where iduser = (?)";
            try (PreparedStatement st = conexao.prepareStatement(sql)) {
                st.setInt(1, cl.getIdUser());
            }
        }
    } 
    
    public static ArrayList<Cliente> selectAll() throws SQLException{
        Persistencia per = new Persistencia();
        Connection conexao = per.getConexao();
        ArrayList<Cliente> clientes = null;
        
        Statement st = conexao.createStatement();
        ResultSet rs = st.executeQuery("select * from cliente");
        
        while(rs.next()){
            Cliente cl = new Cliente();
            cl.setIdUser(rs.getInt("iduser"));
            cl.setCpf(rs.getString("cpf"));
            cl.setDtNascimento(rs.getString("dtNascimento"));
            cl.setSexo(rs.getString("sexo").charAt(0));
            clientes.add(cl);
        }
        return clientes;
    }
    
    public int update(Cliente cl) throws SQLException{
        Persistencia per = new Persistencia();
        int qt;
        try (Connection conexao = per.getConexao()) {
            String sql = "update cliente"+
                    "set cpf=?, sexo=?, nome=?, dtNascimento=?"+
                    "where iduser = ?";
            try (PreparedStatement st = conexao.prepareStatement(sql)) {
                st.setString(1, cl.getCpf());
                st.setString(2, String.valueOf(cl.getSexo()));
                st.setString(3, cl.getNome());
                st.setString(4, cl.getDtNascimento());
                qt = st.executeUpdate();
            }
        }
        return qt;        
    }
}
