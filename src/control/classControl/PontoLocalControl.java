/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control.classControl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Persistencia;
import model.domain.PontoLocal;

/**
 *
 * @author daniel
 */
public class PontoLocalControl {
    public static int insert(PontoLocal pl) throws SQLException{
        Persistencia per = new Persistencia();
        int qt;
        try (Connection conexao = per.getConexao()) {
            String sql = "insert into vendas.pontolocal (idponto, endereco) values (?,?)";
            try (PreparedStatement st = conexao.prepareStatement(sql)) {
                st.setInt(1, 1);
                st.setString(2, pl.getEndereco());
                qt = st.executeUpdate();
            }
        }
        return qt;
    }
    
    public static void remove(PontoLocal pl) throws SQLException{
        Persistencia per = new Persistencia();
        try (Connection conexao = per.getConexao()) {
            String sql = "delete from vendas.pontolocal where idlocal = (?)";
            try (PreparedStatement st = conexao.prepareStatement(sql)) {
                st.setInt(1, pl.getId());
            }
        }
    } 
    
    public static ArrayList<PontoLocal> selectAll() throws SQLException{
        Persistencia per = new Persistencia();
        Connection conexao = per.getConexao();
        ArrayList<PontoLocal> pontos = null;
        
        Statement st = conexao.createStatement();
        ResultSet rs = st.executeQuery("select * from pontocliente");
        
        while(rs.next()){
            PontoLocal ponto = new PontoLocal();
            ponto.setEndereco(rs.getString("endereco"));
            ponto.setId(rs.getInt("idlocal"));
            pontos.add(ponto);
        }
        return pontos;
    }
    
    public int update(PontoLocal pl) throws SQLException{
        Persistencia per = new Persistencia();
        int qt;
        try (Connection conexao = per.getConexao()) {
            String sql = "update vendas.pontolocal"+
                    "set endereco = ?"+
                    "where idlocal = ?";
            try (PreparedStatement st = conexao.prepareStatement(sql)) {
                st.setInt(1, pl.getId());
                st.setString(2, pl.getEndereco());
                qt = st.executeUpdate();
            }
        }
        return qt;        
    }
}
