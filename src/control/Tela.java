/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import javax.swing.JFrame;
import javax.swing.JPanel;
import view.MenuPrincipal;
import view.menus.VeiculosMenuView;
import view.paineis.CadastroClienteView;
import view.paineis.CadastroPontoLocalView;
import view.paineis.TodasQueriesView;

/**
 *
 * @author Daniel
 */
public abstract class Tela {
    
    private static JFrame frame;
    private static  JFrame jf;
        
    public static void inicializaTela(){
        
        frame= new JFrame("Ingressaria");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(700, 400);
        frame.setResizable(false);
        frame.add(new MenuPrincipal());
        frame.setVisible(true);
    }
    
    public static void atualizaTela(String tela){
        JPanel painel = null;
        switch (tela){
           case "TodasQueries": 
               painel = new TodasQueriesView();
               break;
           case "CadastroPonto":
               painel = new CadastroPontoLocalView();
               break;
           case "CadastroCliente":
               painel = new CadastroClienteView();
               break;
           case "CadastrarVeiculo":
           case "ConsultarVeiculo":
               painel = new VeiculosMenuView();
               break;
       } 

        //Tela.frame.setContentPane(painel);
        frame.setContentPane(painel);
        Tela.frame.repaint();
        Tela.frame.validate();
    }
        
    
    public static void start(){
        
        frame = new JFrame("Rock In Rio");
        frame.setSize(400, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(new MenuPrincipal());
        frame.setVisible(true);
    
    }
    
    public static void end(){
    frame.dispose();
    }


}
